package notifiers

import (
	"codeberg.org/momar/chihuahua"
	"context"
	"github.com/rs/zerolog/log"
	"os"
	"os/exec"
	"strings"
	"time"
)

type CommandNotifier struct {
	Command string `hcl:"command"`
}

func init() {
	chihuahua.Notifiers["command"] = &CommandNotifier{}
}

func (c *CommandNotifier) Notify(cfg *chihuahua.Config, check chihuahua.Check, previous chihuahua.CheckResult) {
	ctx, cancel := context.WithTimeout(context.Background(), 30 * time.Second)
	defer cancel()
	cmd := exec.CommandContext(ctx, "sh", "-c", c.Command)

	cmd.Env = append(os.Environ(),
		"CHECK_PARENT=" + strings.Join(check.Parent.FullID(), "/"),
		"CHECK_NAME=" + check.Name,
		"CHECK_RESULT=" + check.Result.Status.String(),
		"CHECK_PREVIOUS_RESULT=" + previous.Status.String(),
		"CHECK_DETAILS=" + check.Result.Details,
		"CHECK_ERROR=" + check.Result.Error,
	)

	if output, err := cmd.CombinedOutput(); err == nil {
		log.Debug().Bytes("output", output).Str("command", c.Command).Msg("Executed notification command")
	} else {
		log.Error().Bytes("output", output).Str("command", c.Command).Err(err).Msg("Executed notification command")
	}
}
