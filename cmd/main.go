package main

import (
	"codeberg.org/momar/chihuahua"
	"codeberg.org/momar/chihuahua/config"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"

	"bufio"
	"github.com/gin-gonic/gin"
	"github.com/teris-io/cli"
	"net/http"
	"net"

	// Register notifiers
	_ "codeberg.org/momar/chihuahua/notifiers"
)

func main() {
	app := cli.New("the smallest watchdog on earth")

	app.WithOption(cli.NewOption("config", "set the path to the configuration file").WithChar('c').WithType(cli.TypeString)).
		WithOption(cli.NewOption("once", "only run checks once and print the result - doesn't start the webserver").WithType(cli.TypeBool)).
		WithOption(cli.NewOption("silent", "disables notifications - use with care!").WithType(cli.TypeBool)).
		WithOption(cli.NewOption("debug", "enable debugging output").WithType(cli.TypeBool)).
		WithAction(func(args []string, options map[string]string) int {
			if options["debug"] == "" {
				log.Logger = log.Logger.Level(zerolog.InfoLevel)
			}

			// Parse the config
			cfg := config.Setup(options["config"], options["once"] == "")

			// Remove notifiers if --silent is given
			if options["silent"] != "" {
				log.Warn().Msg("notifications are disabled as --silent is given")
				cfg.Silent = true
				cfg.Notifiers = map[string]chihuahua.NotifierConfig{}
			}

			// Only run once if --once is given
			if options["once"] != "" {
				errs := chihuahua.RunOnce(cfg)
				chihuahua.GetCheck(cfg)(&gin.Context {
					Request: &http.Request{},
					Writer: ginConsoleWriter{},
				})
				if errs != nil {
					os.Exit(2)
				} else {
					os.Exit(0)
				}
			}

			// Start scheduler & API/webserver otherwise
			chihuahua.GenerateKeys()
			chihuahua.Schedule(cfg)
			chihuahua.Api(cfg)

			return 0
		})

	os.Exit(app.Run(os.Args, os.Stdout))
}

type ginConsoleWriter struct {}
func (ginConsoleWriter) Header() http.Header { return http.Header{} }
func (ginConsoleWriter) WriteHeader(int) {}
func (ginConsoleWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) { return nil, nil, nil }
func (ginConsoleWriter) Flush() {}
func (ginConsoleWriter) CloseNotify() <-chan bool { return nil }
func (ginConsoleWriter) Status() int { return 0 }
func (ginConsoleWriter) Size() int { return 0 }
func (ginConsoleWriter) WriteString(s string) (int, error) {
	return os.Stdout.Write([]byte(s))
}
func (ginConsoleWriter) Written() bool { return false }
func (ginConsoleWriter) WriteHeaderNow() {}
func (ginConsoleWriter) Pusher() http.Pusher { return nil }
func (ginConsoleWriter) Write(b []byte) (int, error) {
	return os.Stdout.Write(b)
}
