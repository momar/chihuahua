package notifiers

import (
	"bytes"
	"codeberg.org/momar/chihuahua"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type GotifyNotifier struct {
	Server   string `hcl:"server"`
	Token    string `hcl:"token"`
	Priority *int   `hcl:"priority"`
}

func init() {
	chihuahua.Notifiers["gotify"] = &GotifyNotifier{}
}

func (n *GotifyNotifier) Notify(cfg *chihuahua.Config, check chihuahua.Check, previous chihuahua.CheckResult) {
	priority := 8
	if n.Priority != nil {
		priority = *n.Priority
	}

	body, err := json.Marshal(map[string]interface{}{
		"title":    "[" + check.Result.Status.String() + "] " + strings.Join(check.FullName(), " » ") + " (previously " + previous.Status.String() + ")",
		"message":  strings.TrimSpace(check.Result.Details + "\n" + check.Result.Error),
		"priority": priority,
		// TODO: extras.client::display.contentType = text/markdown
	})
	if err != nil {
		log.Error().Str("id", strings.Join(check.FullID(), "/")).Err(err).Msg("couldn't send notification through gotify")
		// TODO: ScheduleRetry(n, check, previous)
		return
	}

	res, err := http.Post(strings.TrimSuffix(n.Server, "/")+"/message?token="+url.QueryEscape(n.Token), "application/json", bytes.NewReader(body))
	if err != nil || res.StatusCode != 200 {
		data := []byte{}
		status := "invalid"
		if res != nil {
			if res.Body != nil {
				data, _ = ioutil.ReadAll(res.Body)
			}
			status = res.Status
		}
		log.Error().Str("id", strings.Join(check.FullID(), "/")).Err(err).Str("status", status).Bytes("body", data).Msg("couldn't send notification through gotify")
		// TODO: ScheduleRetry(n, check, previous)
		return
	}
	log.Trace().Str("id", strings.Join(check.FullID(), "/")).Msg("notification has been sent through gotify")
}
