#!/bin/sh
set -eu

if which apt-get >/dev/null 2>&1; then
  # Ubuntu
  pkginstall="apt-get install -y"
  pkgremove="apt-get remove --auto-remove"
elif which apk >/dev/null 2>&1; then
  # Alpine Linux
  pkginstall="apk add"
  pkgremove="apk del"
# TODO: CentOS
fi
if [ -z "$pkginstall" -o -z "$pkgremove" ]; then
  echo "You're using an unsupported system. Currently supported by this script:"
  echo " - Debian/Ubuntu"
  echo " - Alpine Linux"
  exit 1
fi

# Install basic monitoring-plugins
$pkginstall monitoring-plugins

# Create user "chihuahua" and add authorized SSH keys
useradd -M -d /var/chihuahua -r -s /bin/sh chihuahua || true
mkdir -p /var/chihuahua/.ssh
echo 'export PATH="/var/chihuahua:/usr/lib/monitoring-plugins:/usr/lib/nagios/plugins:/usr/local/bin:/usr/bin:/bin"' > /var/chihuahua/.chihuahuarc
cat <<'EOF' > /var/chihuahua/.ssh/authorized_keys
[CHIHUAHUA_PUBLIC_KEYS]
EOF

# Install additional monitoring-plugins
pluginspath="$([ -f /usr/lib/monitoring-plugins/check_dummy ] && echo /usr/lib/monitoring-plugins || echo /usr/lib/nagios/plugins)"
curl -L https://raw.githubusercontent.com/hugme/Nag_checks/master/check_linux_memory -o $pluginspath/check_memory
curl -L https://raw.githubusercontent.com/duffycop/nagios_plugins/master/plugins/check_service -o $pluginspath/check_service
chmod +x $pluginspath/check_memory $pluginspath/check_service
if which docker >/dev/null && which pip3 >/dev/null; then
  pip3 install check_docker
  printf '#!/bin/sh\nexec "'"$(which check_docker)"'" "$@"\n' > $pluginspath/check_docker
  chmod +x $pluginspath/check_docker
fi

# Add "check_sudo" script to securely run checks as root
# With this script, applications starting with "check_" in /usr/lib/monitoring-plugins and /usr/lib/nagios/plugins can be run by the "chihuahua" user.
cat <<'EOF' > /usr/local/bin/check_sudo
#!/bin/sh
[ $# -gt 0 ] || { echo "Usage: sudo /usr/local/bin/check_sudo check_... ..."; exit 3; }
[ $(id -u) -eq 0 ] || exec sudo /usr/local/bin/check_sudo "$@"
cmd=$(realpath --canonicalize-existing --no-symlinks "/usr/lib/monitoring-plugins/$1" "/usr/lib/nagios/plugins/$1" 2>/dev/null | grep --max-count 1 -E -e '^/usr/lib/(monitoring-plugins|nagios/plugins)/check_') || { echo "Not a monitoring plugin."; exit 3; }
shift
exec "$cmd" "$@"
EOF
chmod +x /usr/local/bin/check_sudo
grep chihuahua /etc/sudoers >/dev/null || { echo 'chihuahua ALL=(root) NOPASSWD: /usr/local/bin/check_sudo' >> /etc/sudoers; }

chown -R chihuahua:chihuahua /var/chihuahua
echo Setup complete. Run the following commands to remove the Chihuahua access again:
echo "  userdel -r chihuahua"
echo "  $pkgremove monitoring-plugins"
echo "  rm /usr/local/bin/check_sudo"
echo "  sed -i '/^chihuahua /d' /etc/sudoers"
