package notifiers

import (
	"codeberg.org/momar/chihuahua"
	"fmt"
	"strings"
)

type ConsoleNotifier struct{}

func init() {
	chihuahua.Notifiers["console"] = &ConsoleNotifier{}
}

func (*ConsoleNotifier) Notify(cfg *chihuahua.Config, check chihuahua.Check, previous chihuahua.CheckResult) {
	var status string
	switch check.Result.Status {
	case chihuahua.StatusOk:
		status = "\033[1;42m       OK \033[0m"
	case chihuahua.StatusWarning:
		status = "\033[1;43m  WARNING \033[0m"
	case chihuahua.StatusCritical:
		status = "\033[1;41m CRITICAL \033[0m"
	default: // types.StatusUnknown
		status = "\033[1;47m"
	}

	info := strings.Trim(check.Result.Details, "\n")
	if check.Result.Error != "" {
		info += "\n\033[31m" + strings.Trim(check.Result.Error, "\n") + "\033[0m"
	}
	info = strings.Trim(info, "\n")
	if strings.TrimSpace(info) != "" {
		info = "\n" + info
	} else {
		info = ""
	}

	fmt.Printf("%s %s%s\n", status, check.FullID(), info)
}
