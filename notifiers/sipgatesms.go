package notifiers

import (
	"bytes"
	"codeberg.org/momar/chihuahua"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
	"strings"
)

type SipgateSMSNotifier struct {
	Email string `hcl:"email"`
	Password string `hcl:"password"`
	To []string `hcl:"to"`
}

const smsURL = "https://api.sipgate.com/v2/sessions/sms"

func init() {
	chihuahua.Notifiers["sipgatesms"] = &SipgateSMSNotifier{}
}

func (c *SipgateSMSNotifier) Notify(cfg *chihuahua.Config, check chihuahua.Check, previous chihuahua.CheckResult) {
	for _, recipient := range c.To {
		body := map[string]string{
			"smsId": "s0",
			"recipient": recipient,
			"message": Content(cfg, check, previous),
		}

		j, err := json.Marshal(body)
		if err != nil {
			log.Error().Err(err).Msg("Map to JSON failed")
		}
		log.Trace().RawJSON("body", j).Msg("Req JSON")
		req, err := http.NewRequest("POST", smsURL, bytes.NewBuffer(j))
		req.SetBasicAuth(c.Email, c.Password)
		req.Header.Add("Accept", "application/json")
		req.Header.Add("content-type", "application/json")

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Error().Err(err).Msg("couldn't send notification through sipgate. Network error")
			return
		}
		if res != nil {
			data := []byte{}
			if res.Body != nil {
				data, _ = ioutil.ReadAll(res.Body)
				log.Trace().Bytes("Body", data).Int("status", res.StatusCode).Msg("SMS body")
			}
			if res.StatusCode == 204 {
				log.Trace().Str("Recipient", recipient).Msg("SMS send")
				continue
			}
			if res.StatusCode == 401 {
				log.Error().Str("User", c.Email).Str("Passwort", c.Password).Msg("Sipgate Login is wrong! Aborting sending")
				return
			}
			if res.StatusCode == 402 {
				log.Error().Str("User", c.Email).Msg("There is not enough money on your account! Aborting sending")
				return
			}
			log.Error().Bytes("Body", data).Msgf("Undefined behaviour; %+v", res)
			continue
		}
		log.Error().Msg("No network error and no response body! Undefined behaviour in sipgate api")
	}
}

func Content(cfg *chihuahua.Config, check chihuahua.Check, previous chihuahua.CheckResult) string {
	var msg = "Status Change: " + check.Name + "\n"

	msg += "Check is now " + check.Result.Status.String() + " (was " + previous.Status.String() + ")\n"
	if cfg.RootURL != "" {
		msg += cfg.RootURL + "\n"
	}
	if check.Result.Error != "" {
		msg += "\n" + check.Result.Error
	}
	if len(msg) > 459 {
		msg = msg[:455] + " ..."
	}
	msg = strings.TrimSuffix(msg, "\n")
	return msg
}
