package config

import (
	"codeberg.org/momar/chihuahua"
	"errors"
	"github.com/antonmedv/expr"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/rs/zerolog/log"
	"reflect"
	"strings"
	"time"
)

func New() *chihuahua.Config {
	return &chihuahua.Config{
		Servers:   []*chihuahua.ServerOrGroup{},
		Notifiers: map[string]chihuahua.NotifierConfig{},
	}
}

// TODO: lots of variable names just don't make any sense at all!
// TODO: better error messages (most are good already but the error position is missing for some)
// TODO: check if references vs. values are correctly handled

func Parse(path string, cfg *chihuahua.Config) error {
	if path == "" {
		path = "/etc/chihuahua.hcl"
	}
	src := &Config{}
	err := hclsimple.DecodeFile(path, nil, src)
	if err != nil {
		return err
	}
	log.Trace().Msg("configuration has been parsed by HCL")

	// parse notifiers, but don't apply them yet (so applying doesn't throw errors)
	notifierStructs := map[string]chihuahua.NotifierConfig{}
	for _, notifierConfig := range src.Notifier {
		if providerType, ok := chihuahua.Notifiers[notifierConfig.Type]; ok {
			var provider = reflect.New(reflect.TypeOf(providerType).Elem()).Interface()
			diag := gohcl.DecodeBody(notifierConfig.Remain, nil, provider)
			if diag.HasErrors() {
				return errors.New(diag.Error())
			}

			filters := []chihuahua.Filter{}
			if notifierConfig.Filters != nil {
				for _, f := range notifierConfig.Filters {
					prg, err := expr.Compile(f.If)
					if err != nil {
						return err // TODO:
					}
					filters = append(filters, chihuahua.Filter{
						If: prg,
						Accept: f.Accept,
					})
				}
			}
			notifierStructs[notifierConfig.ID] = chihuahua.NotifierConfig{
				Notifier: provider.(chihuahua.Notifier),
				Filters: filters,
			}
		} else {
			mir := notifierConfig.Remain.MissingItemRange()
			notifiers := []string{}
			for id, _ := range chihuahua.Notifiers {
				notifiers = append(notifiers, id)
			}
			return errors.New(mir.String() + ": Invalid value; The argument \"type\" must be a valid notifier type (available types: " + strings.Join(notifiers, ", ") + ").")
		}
	}

	// parse intervals recursively
	var parseIntervals func(serversOrGroups []ServerOrGroup) error
	parseIntervals = func(serversOrGroups []ServerOrGroup) error {
		for i, serverOrGroup := range serversOrGroups {
			if serverOrGroup.Interval != nil {
				interval, err := time.ParseDuration(*serverOrGroup.Interval)
				if err != nil {
					return err // TODO: print position
				}
				serversOrGroups[i].interval = &interval
			}
			if serverOrGroup.Check != nil {
				for j, chk := range serverOrGroup.Check {
					if chk.Interval != nil {
						interval, err := time.ParseDuration(*chk.Interval)
						if err != nil {
							return err // TODO: print position
						}
						serversOrGroups[i].Check[j].interval = &interval
					}
					if chk.Notify != nil {
						for _, notifierID := range *chk.Notify {
							if _, ok := notifierStructs[notifierID]; !ok {
								return errors.New("notifier not found: " + notifierID) // TODO: print position
							}
						}
					}
				}
			}
			if serverOrGroup.Notify != nil {
				for _, notifierID := range *serverOrGroup.Notify {
					if _, ok := notifierStructs[notifierID]; !ok {
						return errors.New("notifier not found: " + notifierID) // TODO: print position
					}
				}
			}
			if serverOrGroup.Server != nil {
				if err := parseIntervals(serversOrGroups[i].Server); err != nil {
					return err
				}
			}
			if serverOrGroup.Group != nil {
				if err := parseIntervals(serversOrGroups[i].Group); err != nil {
					return err
				}
			}
		}
		return nil
	}
	if src.Interval != nil {
		interval, err := time.ParseDuration(*src.Interval)
		if err != nil {
			return err // TODO: print position
		}
		src.interval = &interval
	}
	if err := parseIntervals(src.Server); err != nil {
		return err
	}
	if err := parseIntervals(src.Group); err != nil {
		return err
	}

	ctx := Context{
		Parent:   []string{},
		Disable:  false,
		Notify:   nil,
		Verify:   0,
		Interval: 5 * time.Minute,
		Checks:   src.Check,
	}
	// apply values from src to ctx
	if src.Disable != nil {
		ctx.Disable = *src.Disable
	}
	if src.Notify != nil {
		ctx.Notify = *src.Notify
		for _, notifierID := range ctx.Notify {
			if _, ok := notifierStructs[notifierID]; !ok {
				return errors.New("notifier not found: " + notifierID) // TODO: print position
			}
		}
	}
	if src.Verify != nil {
		ctx.Verify = *src.Verify
	}
	if src.interval != nil {
		ctx.Interval = *src.interval
	}

	// merge server & group array to keep the order from the configuration file
	src.Group, err = Merge(src.Server, src.Group)
	if err != nil {
		return err
	}
	src.Server = nil

	// FROM THIS POINT, NO ERROR SHALL BE THROWN!

	// apply the server/group config to the target configuration
	tmp := &chihuahua.ServerOrGroup{Children: cfg.Servers}
	Apply(src.Group, ctx, tmp)
	cfg.Servers = tmp.Children // if the reference has changed, e.g. by append(), override it

	notifierIDs := map[string]struct{}{}

	// apply notifiers (which have been parsed before so now there will be no error)
	for id, notifier := range notifierStructs {
		providerOld, okOld := chihuahua.Notifiers[id]
		if okOld {
			ctxOld, okOld := providerOld.(chihuahua.NotifierWithContext)
			ctxNew, okNew := notifier.Notifier.(chihuahua.NotifierWithContext)
			if _, ok := cfg.Notifiers[id]; !ok || reflect.TypeOf(cfg.Notifiers[id].Notifier) != reflect.TypeOf(notifier.Notifier) || !okOld || !okNew {
				// modify existing one without context
				cfg.Notifiers[id] = notifier
			} else {
				// modify existing notifier with context
				ctxNew.Import(ctxOld.Export())
				notifier.Notifier = ctxNew
				cfg.Notifiers[id] = notifier
			}
		} else {
			// add new notifier
			cfg.Notifiers[id] = notifier
		}
		notifierIDs[id] = struct{}{}
	}
	// remove stray notifiers
	for id := range cfg.Notifiers {
		if _, ok := notifierIDs[id]; !ok {
			delete(cfg.Notifiers, id)
		}
	}

	// apply root URL
	if src.RootURL != nil {
		cfg.RootURL = *src.RootURL
	} else {
		cfg.RootURL = ""
	}

	log.Trace().Interface("config", cfg).Msg("configuration has been updated")
	return nil
}

func (in Check) Convert(out *chihuahua.Check, ctx Context) {
	if in.Disable != nil {
		ctx.Disable = *in.Disable
	}
	if in.Notify != nil {
		ctx.Notify = *in.Notify
	}
	if in.Verify != nil {
		ctx.Verify = *in.Verify
	}
	if in.interval != nil {
		ctx.Interval = *in.interval
	}

	out.Disable = ctx.Disable
	if ctx.Notify != nil {
		out.Notifiers = ctx.Notify[:]
	} else {
		out.Notifiers = nil
	}
	out.Verify = ctx.Verify
	out.Interval = ctx.Interval
	out.Name = in.ID
	if in.Name != nil {
		out.Name = *in.Name
	}
	out.ID = in.ID
	out.Command = in.Command
	if out.Result.LastUpdate.IsZero() {
		out.Result.Status = chihuahua.StatusUnknown
		out.Result.LastUpdate = time.Now()
		out.Result.Details = "Waiting for first check..."
	}
}

func Apply(in []ServerOrGroup, ctx Context, out *chihuahua.ServerOrGroup) {
	childIndices := map[string]int{}
	for i, child := range out.Children {
		childIndices[child.ID] = i
	}
	childIDs := map[string]struct{}{}

	for _, src := range in {
		childIDs[src.ID] = struct{}{}

		var res *chihuahua.ServerOrGroup
		if i, ok := childIndices[src.ID]; ok {
			res = out.Children[i]
		} else {
			res = &chihuahua.ServerOrGroup{
				ID:               src.ID,
				Name:             src.ID,
				ConnectionType:   "",
				ConnectionParams: "",
				Checks:           []*chihuahua.Check{},
				Children:         []*chihuahua.ServerOrGroup{},
				Parent:           out,
			}
			out.Children = append(out.Children, res)
		}
		res.Parent = out

		if src.Name != nil {
			res.Name = *src.Name
		}

		checks := append(ctx.Checks, src.Check...)
		ctx2 := Context{
			Disable:  ctx.Disable,
			Notify:   ctx.Notify,
			Verify:   ctx.Verify,
			Interval: ctx.Interval,
			Checks:   checks,
		}
		if src.Disable != nil {
			ctx2.Disable = *src.Disable
		}
		if src.Notify != nil {
			ctx2.Notify = *src.Notify
		}
		if src.Verify != nil {
			ctx2.Verify = *src.Verify
		}
		if src.interval != nil {
			ctx2.Interval = *src.interval
		}

		if src.connectionType != "" {
			res.Children = nil // we're in a server now!

			res.ConnectionType = src.connectionType
			res.ConnectionParams = *src.Connection

			checkIndices := map[string]int{}
			for i, chk := range res.Checks {
				checkIndices[chk.ID] = i
			}
			log.Debug().Interface("indices", checkIndices).Interface("checks", res.Checks).Send()
			checkIDs := map[string]struct{}{}

			for _, chk := range checks {
				checkIDs[chk.ID] = struct{}{}
				if i, ok := checkIndices[chk.ID]; ok {
					chk.Convert(res.Checks[i], ctx2)
					res.Checks[i].Parent = res
				} else {
					chkOut := &chihuahua.Check{}
					chk.Convert(chkOut, ctx2)
					chkOut.Parent = res
					res.Checks = append(res.Checks, chkOut)
					checkIndices[chk.ID] = len(res.Checks) - 1
				}
			}
			for i := 0; i < len(res.Checks); i++ {
				if _, ok := checkIDs[res.Checks[i].ID]; !ok {
					if res.Checks[i].Job != nil {
						res.Checks[i].Job.Quit <- true
					}
					res.Checks = append(res.Checks[:i], res.Checks[i+1:]...)
					i--
				}
			}
		} else {
			res.Checks = nil // we're in a group now

			Apply(src.Group, ctx2, res)
		}
	}
	var stopChecks = func(s *chihuahua.ServerOrGroup) {
		if s.Checks != nil {
			for _, chk := range s.Checks {
				if chk.Job != nil {
					chk.Job.Quit <- true
				}
			}
		}
	}
	for i := 0; i < len(out.Children); i++ {
		if _, ok := childIDs[out.Children[i].ID]; !ok {
			stopChecks(out.Children[i])
			out.Children = append(out.Children[:i], out.Children[i+1:]...)
			i--
		}
	}
}

func Merge(servers []ServerOrGroup, groups []ServerOrGroup) ([]ServerOrGroup, error) {
	result := make([]ServerOrGroup, len(servers)+len(groups))
	var serversPos, groupsPos = 0, 0
	var err error
	for i := 0; i < len(result) && (serversPos < len(servers) || groupsPos < len(groups)); i++ {
		if serversPos >= len(servers) || (groupsPos < len(groups) && servers[serversPos].Info.MissingItemRange().Start.Line > groups[groupsPos].Info.MissingItemRange().Start.Line) {
			// Next one is the group
			group := groups[groupsPos]

			// check for server-only fields
			if group.Connection != nil {
				mir := group.Info.MissingItemRange()
				return nil, errors.New(mir.String() + ": Unsupported argument; An argument named \"connection\" is not expected here.")
			}

			// check for remaining, required or invalid fields
			if diag := gohcl.DecodeBody(group.Info, nil, &Empty{}); diag.HasErrors() {
				return nil, errors.New(diag.Error())
			}

			group.Group, err = Merge(group.Server, group.Group)
			if err != nil {
				return nil, err
			}
			group.Server = nil

			result[i] = group
			groupsPos++
		} else {
			// Next one is the server
			server := servers[serversPos]

			// check for group-only and server-required fields
			if server.Connection == nil {
				mir := server.Info.MissingItemRange()
				return nil, errors.New(mir.String() + ": Missing required argument; The argument \"connection\" is required, but no definition was found.")
			}
			connectionParts := strings.SplitN(*server.Connection, " ", 2)
			server.connectionType = connectionParts[0]
			if len(connectionParts) > 1 {
				*server.Connection = strings.TrimSpace(connectionParts[1])
			} else {
				*server.Connection = ""
			}
			if server.connectionType != "local" && server.connectionType != "ssh" && server.connectionType != "push" {
				mir := server.Info.MissingItemRange()
				return nil, errors.New(mir.String() + ": Invalid value; The argument \"connection\" must start with \"local\", \"ssh\" or \"push\".")
			}
			if server.connectionType == "ssh" && strings.TrimSpace(*server.Connection) == "" {
				mir := server.Info.MissingItemRange()
				return nil, errors.New(mir.String() + ": Invalid value; The argument \"connection\" must contain additional parameters when starting with \"ssh\" (e.g. \"ssh -i /tmp/id_rsa root@example.org\").")
			}

			if server.Connection == nil {
				mir := server.Info.MissingItemRange()
				return nil, errors.New(mir.String() + ": Missing required argument; The argument \"connection\" is required, but no definition was found.")
			}
			if server.Group != nil && len(server.Group) > 0 {
				mir := server.Info.MissingItemRange()
				return nil, errors.New(mir.String() + ": Unsupported block type; Blocks of type \"group\" are not expected here.")
			}
			if server.Server != nil && len(server.Server) > 0 {
				mir := server.Info.MissingItemRange()
				return nil, errors.New(mir.String() + ": Unsupported block type; Blocks of type \"server\" are not expected here.")
			}

			// check for remaining, required or invalid fields
			if diag := gohcl.DecodeBody(server.Info, nil, &Empty{}); diag.HasErrors() {
				return nil, errors.New(diag.Error())
			}

			server.Info = nil

			server.Group = nil
			server.Server = nil

			result[i] = server
			serversPos++
		}
	}
	return result, nil
}
