package main

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"path"
	"runtime"
	"strings"
	"time"
	"strconv"
)

var IgnoreCallers = []string{
	"main.GetCaller",
	"main.init.0.func1",
}
var IgnorePackages = []string{
	"github.com/rs/zerolog",
	"github.com/rs/zerolog/log",
}

func sliceContains(s []string, v string) bool {
	for _, x := range s { if x == v { return true } }
	return false
}

func GetCaller() string {
	// TODO: automatically add timer for JSON logging
	programCounters := make([]uintptr, 15)
	n := runtime.Callers(0, programCounters)
	if n > 0 {
		frames := runtime.CallersFrames(programCounters[:n])
		for more := true; more; {
			var frameCandidate runtime.Frame
			frameCandidate, more = frames.Next()
			pkg := strings.TrimPrefix(path.Dir(frameCandidate.Function) + "/" + strings.SplitN(path.Base(frameCandidate.Function), ".", 2)[0], "./")
			if frameCandidate.Function != "runtime.Callers" && (strings.Contains(pkg, "/") || pkg == "main") && !sliceContains(IgnorePackages, pkg) && !sliceContains(IgnoreCallers, frameCandidate.Function) {
				return frameCandidate.Function + " [" + path.Base(frameCandidate.File) + ":" + strconv.Itoa(frameCandidate.Line) + "]"
			}
		}
	}
	return ""
}

func init() {
	if os.Getenv("LOG") != "" && os.Getenv("LOG") != "console" {
		return
	}
	cw := zerolog.NewConsoleWriter()
	if strings.Contains("\t" + strings.Join(os.Args[1:], "\t") + "\t", "\t--once\t") {
		cw.Out = os.Stderr
	}
	cw.FormatCaller = func(i interface{}) string {
		if i != nil {
			return fmt.Sprintf("\x1b[%sm%v\x1b[0m", "1;36", i)
		}
		c := GetCaller()
		if c != "" {
			return fmt.Sprintf("\x1b[%sm%v\x1b[0m", "1;36", strings.SplitN(path.Base(c), ".", 2)[0])
		} else {
			return ""
		}
	}
	cw.FormatTimestamp = func(i interface{}) string {
		return fmt.Sprintf("\x1b[%sm%v\x1b[0m", "90", time.Now().UTC().Format("2006-01-02 15:04:05 "))
	}
	log.Logger = zerolog.New(cw)
}
